"""
Data for the oloid.

As a solid, this surface rolls with a meandering motion. Its contact
with the floor is always a line segment of length sqrt(3), which
makes parametrising the surface easy.

N = number of segments in a 1/4 circle (the oloid being the convex hull
    of two such circles)
"""
from mpmath import *
from malibu.trigens import bilinear
from malibu.symmetry.generators import permu
from malibu.symmetry import pointgroup

def prepatch(**others):
    def patch(w):
        th, l = w.real, w.imag
        p = expj(th)
        if l == 0:
            return matrix([p.real + 0.5, p.imag, 0])
        cp = p.real / (p.real + 1)
        return matrix([cp - 0.5, 0, sqrt(1-cp*cp)])
    return patch

def trigen(*, N=32, **others):
    yield from bilinear(0, pi/2, 1j, pi/2+1j, N, 1)

def syms(*, k=2, **others):
    return pointgroup("Dd", 2, basis=permu(-3, 2, 1))
