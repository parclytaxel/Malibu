"""
Data for a plain torus, explicitly treated as a surface of revolution.

This is called exactly the same way as the (original) torus module.
"""
from mpmath import pi, rect
from .revolution import prepatch as pp, trigen as tg

def prepatch(*, R, r, **others):
    curve = lambda t: R + rect(r, t)
    return pp(curve=curve, **others)
def trigen(**others):
    return tg(curve_a=0, curve_b=2*pi, **others)
