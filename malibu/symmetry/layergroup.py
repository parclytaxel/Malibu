"""
Layer groups (3D space groups with a 2D lattice).

These are isomorphic to magnetic wallpaper groups and are named as such
in Malibu, using a "BNS convention".

Colourless groups are simply the short name of the normal wallpaper group,
except that p31m and p3m1 are abbreviated by dropping the last letter.
Grey groups have a t suffix (for "time reversal", which becomes flipping
in the z-axis in the context of layer groups).

Each wallpaper group has at most three associated type III groups, where
the two colours are not related by a translation. They get an x, y or z suffix
depending on the relative ordering of the one-colour subgroups in the
ITC ordering of the wallpaper groups:

p1, p2, pm, pg, cm, pmm, pmg, pgg, cmm, p4, p4m, p4g, p3, p3m, p31, p6, p6m

For example:
p-4b2 -> p4gx (pgg is one-colour subgroup)
p-42_1m -> p4gy (cmm)
p42_12 -> p4gz (p4)

If the two colours ARE related by a translation, the layer group is type IV
and receives an a, b or n suffix depending on the translation direction.
a means that it is parallel to a reflection or glide (with reflection
taking precedence), b means perpendicular and n means a and b together.
"""
import re
from mpmath import eye
from .transforms import promote, mprod, changebase_deco, rotate_z, permu
from .pointgroup import pointgroup

non_hm_symbols = re.compile(r"[^1-46_/abcemnp-]")

ops = {
"p1": [("C", 1)],
"p1a": None,

"p2": [("C", 2)]
}

hm_to_wp = {
"p1": "p1",
"p11m": "p1t",
"p11a": "p1a",
"p112": "p2",
"p112/m": "p2t",
"p-1": "p2x",
"p112/a": "p2a"
}

num_to_wp = ["p1", "p2x", "p2", "p1t"]

@changebase_deco
def layergroup(kind, a=1, b=1):
    """Yield matrices describing the specified layer group.
    
    kind is an integer in [1, 80], a wallpaper-based descriptor
    as described in the module documentation or a Hermann–Mauguin symbol.
    The implied unit cell and from there the meaning of a and b differ by
    the underlying wallpaper group:
    
    p1, p2: a and b are vectors (complex numbers) defining a parallelogram
    p3*, p6*: no layer groups of these types require a or b
    p4*: a is side length of a square
    all others: a and b are width and height of a rectangle/rhombus
    
    All these cells are origin-centred."""
    if type(kind) == int:
        kind = num_to_wp[kind-1]
    if kind.endswith("t"):
        yield from mprod(pointgroup("Ch", 1), layergroup(kind[:-1]))
        return
    if kind not in ops:
        kind = hm_to_wp[non_hm_symbols.sub("", kind.lower())]
