A [line group](https://en.wikipedia.org/wiki/Line_group) is a 3D space group whose translation lattice is one-dimensional. All elements of such a group thus map an axis to itself, which is canonically taken as the z-axis. They may also be (as noted in Wikipedia) interpreted as wallpaper groups wrapped on a cylinder around the axis.

## Line group notation

Just as there are seven infinite families of 3D point groups, there are also infinitely many line groups and they fall into 13 families. Unlike point groups for which there is Schoenflies notation, there is no common scheme for naming line groups in a manner at least somewhat intuitive, so I made my own for Malibu's purposes.

It is clear that the aforementioned seven infinite families naturally extend to seven types of line groups, each instance of a point group becoming a ring of n unit cells. The other six are derived using two operations:

* The push (p) operation moves every fundamental domain of one handedness by half the ring height
* The zigzag (z) operation staggers the rings, every other ring being rotated by π/n

With these operations the 13 families are then as follows.

| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 |
|---|---|---|---|---|---|---|---|---|----|----|----|----|
| C<sub>n</sub> | S<sub>2n</sub> | C<sub>nh</sub> | C<sub>nv</sub> | D<sub>n</sub> | D<sub>nd</sub> | D<sub>nh</sub> | zC<sub>nh</sub> | pC<sub>nv</sub> | zC<sub>nv</sub> | pD<sub>nd</sub> | pD<sub>nh</sub> | zD<sub>nh</sub> |

The number assigned to each type is the ultimate object that `linegroup()` works with. Based on this scheme, the string descriptor for each type, which `linegroup()` also accepts, is obtained by leaving out n and concatenating the other letters, e.g. type 2 is `S` and type 10 is `zCv`.

## Helices

The above is an _almost_ complete description of the line groups, but not a complete one because the existence of translations allows screw axes to appear. n<sub>q</sub> in Hermann–Mauguin notation means a symmetry of rotating by 2π/n around an axis followed by moving q/n of the unit cell dimension in that direction. But helices don't have to be rational in that sense; DNA and alpha helices in proteins, as well as the Boerdijk–Coxeter helix of regular tetrahedra, are examples of irrational helices.

Line groups associated to helices are of type 1 or 5 and are denoted C<sub>n/q</sub> and D<sub>n/q</sub> in the rational case. To handle all helices, the fourth argument q of `linegroup()` is provided and behaves differently depending on whether it is integral or not.

* If integral, the Hermann–Mauguin symbol n<sub>q</sub> is implemented as described above.
* If not integral, n unit cells will be constructed where the next unit cell is arg(q) counterclockwise and _a_ units up from the previous one. Thus q should be a complex number to avoid a trivial helix.

## Correspondence between systems

While there are infinitely many line groups, only 75 of them may appear inside a crystal and are known as the **rod groups**. They are numbered in the International Tables for Crystallography; the table below gives the mapping between those numbers and my notation together with Hermann–Mauguin symbols. The numbers and symbols are accepted by `rodgroup()`, a wrapper around `linegroup()`.

Some rod groups are geometrically isomorphic in 3D space, but become non-isomorphic when required to fix the axis. Others split into two distinct groups when the lattice dimension is increased because the line may be arbitrarily rotated. Where a range of numbers is given, this is understood as q increasing from 0 at the left end of the range to n−1 at the right.

| Type | n = 1 | n = 2 | n = 3 | n = 4 | n = 6 |
|------|-------|-------|-------|-------|-------|
|    1 | 1, p1 | 8–9, p112–p112<sub>1</sub> | 42–44, p3–p3<sub>2</sub> | 23–26, p4–p4<sub>3</sub> | 53–58, p6–p6<sub>5</sub> |
|    2 | 2, p1&#x305; | 27, p4&#x305; | 45, p3&#x305; |
|    3 | 10, p11m | 11, p112/m | 59, p6&#x305; | 28, p4/m | 60, p6/m |
|    4 | 4, pm11 | 15, pmm2 | 49, p3m1, p31m | 34, p4mm | 68, p6mm |
|    5 | 3, p211 | 13–14, p222–p222<sub>1</sub> | 46–48, p312–p3<sub>2</sub>12, p321–p3<sub>2</sub>21 | 30–33, p422–p4<sub>3</sub>22 | 62–67, p622–p6<sub>5</sub>22 |
|    6 | 6, p2/m11 | 37, p4&#x305;2m, p4&#x305;m2 | 51, p3&#x305;m1, p3&#x305;1m |
|    7 | 18, p2mm | 20, pmmm | 71, p6&#x305;m2, p6&#x305;2m | 39, p4/mmm | 73, p6/mmm |
|    8 | 12, p112<sub>1</sub>/m | 29, p4<sub>2</sub>/m | 61, p6<sub>3</sub>/m |
|    9 | 5, pc11 | 16, pcc2 | 50, p3c1, p31c | 36, p4cc | 69, p6cc |
|   10 | 17, pmc2<sub>1</sub> | 35, p4<sub>2</sub>cm, p4<sub>2</sub>mc | 70, p6<sub>3</sub>mc, p6<sub>3</sub>cm |
|   11 | 7, p2/c11 | 38, p4&#x305;2c, p4&#x305;c2 | 52, p3&#x305;c1, p3&#x305;1c |
|   12 | 19, p2cm | 21, pccm | 72, p6&#x305;c2, p6&#x305;2c | 40, p4/mcc | 74, p6/mcc |
|   13 | 22, pmcm | 41, p4<sub>2</sub>/mmc, p4<sub>2</sub>/mcm | 75, p6<sub>3</sub>/mmc, p6<sub>3</sub>/mcm |

## Preferred positions

`pointgroup()`'s default setting has the rotation required by D<sub>n</sub> about the x-axis and the reflection required by C<sub>nv</sub>/D<sub>nd</sub>/D<sub>nh</sub> in the xz-plane. Orientation and position do not matter with point groups because their matrices form a true mathematical group, but they matter for all other space groups.

Malibu's functions have certain _preferred positions_ for a fundamental domain with respect to each group. A patch in such a position tiles an implied unit cell neatly; other settings are achieved by passing a `basis` keyword argument. For line groups, the implied unit cell is bounded by z = ±a/2 in cylindrical coordinates (i.e. origin-centred). Preferred positions for each line group type are given below in the same coordinate system where k = n/gcd(n,q):

| Type | Preferred position |
|------|--------------------|
| 1 (C<sub>n/q</sub>) | −a/(2k) ≤ z ≤ a/(2k) (k odd), 0 ≤ z ≤ a/k (k even), −πk/n ≤ φ ≤ πk/n |
| 2, 4 | 0 ≤ φ ≤ π/n, −a/2 ≤ z ≤ a/2 |
| 3, 9 | −π/n ≤ φ ≤ π/n, 0 ≤ z ≤ a/2 |
| 5 (D<sub>n/q</sub>) | Same as C<sub>n/q</sub> except 0 ≤ φ ≤ πk/n |
| 6–8, 10–12 | 0 ≤ φ ≤ π/n, 0 ≤ z ≤ a/2 |
| 13 | 0 ≤ φ ≤ π/n, 0 ≤ z ≤ a/2, φ/(π/n)+z/(a/2) ≤ 1 |

These positions are illustrated in `linegroups.svg`. The non-faded unit cell for each group is centred on the x-axis and the preferred position is highlighted in blue. Irrational helices are excluded since they don't have well-defined _translation-only_ unit cells.
