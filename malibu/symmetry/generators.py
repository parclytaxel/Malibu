"""
Generators of transformation matrices.

The functions here differ from constants.py by taking arguments;
they differ from transforms.py by providing actual matrices to work with,
rather than merely processing them.
"""
from mpmath import matrix, eye, root, sign
from .transforms import promote

def rotate_z(v, flipz=1):
    """Return the matrix representing a rotation about the z-axis by arg(v).
    If v = (n, k), the rotation is k/n of a full turn.
    If flipz = -1, a reflection in the xy-plane is added."""
    try:
        z = root(1, v[0], v[1])
    except TypeError:
        z = sign(v)
    return matrix([[z.real, -z.imag, 0], [z.imag, z.real, 0], [0, 0, flipz]])

def cyclic_z(n, flipz=1):
    """If flipz = 1, yield matrices representing Schoenflies's C_n, i.e.
    n evenly spaced rotations about the z-axis. If flipz = -1,
    yield (for even n) Schoenflies's S_n instead."""
    yield from (rotate_z((n, k), flipz**k) for k in range(n))

def permu(*cols):
    """If cols is the sequence {sign_i sigma_i},
    return a sign matrix where the (i, sigma_i)-entry is sign_i.
    This is useful for generating permutation matrices and
    simple reflections."""
    N = len(cols)
    A = matrix(N)
    for i in range(N):
        A[i,abs(cols[i])-1] = sign(cols[i])
    return A

def translate(a=0, b=0, c=0):
    """Return a pure translation matrix by (a, b, c)."""
    return promote(x=a, y=b, z=c)
