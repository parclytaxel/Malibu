"""Data for Karcher's less-symmetric saddle tower.

The Weierstrass–Enneper parametrisation
(see https://minimal.sitehost.iu.edu/research/claynotes.pdf, p. 34) has
f(z) = 1 / (z^k - expjpi(t))(z^k - expjpi(-t))
g(z) = z^(k-1)

t is in (0, 1) and t = 0.5 gives the most symmetric case. (The source
uses phi = pi/k * t instead.) The fundamental domain is the unit sector
from 0 to pi/k punctured at expjpi(t); it is necessary to use
two grids transformed by the same conformal map to avoid that pole.
The surface is embedded iff (per source) 1/2 - 1/k < (k-1)/k * t < 1/2.

Remarkably, the raw equations produce a height of 4*pi/k independent of t
so we scale to make the unit cell height 2 just like the most symmetric case.
The surface approaches k-noids as phi -> 0.

k, cutoff, Nr, Ni = same as in saddletower.py
t = angle factor, see above"""
from mpmath import *
from malibu.trigens import bilinear
from malibu.symmetry import pointgroup
mp.dps = 40

def prepatch(*, k=2, t=0.5, **others):
    A = expjpi(t)
    scale = k/(2*pi)

    # Solution to quad(lambda w: w**l / (w**k - a), [0, z])
    def hg(l, a, z):
        F = fraction(l+1, k)
        return -power(z,l+1) / (a*(l+1)) * hyp2f1(1, F, 1+F, power(z,k)/a)

    def patch(w, **params):
        P1 = hg(0, A, w) - hg(0, 1/A, w)
        P2 = hg(2*k-2, A, w) - hg(2*k-2, 1/A, w)
        x = im(P1 - P2)
        y = abs(re(P1 + P2))
        wk = power(w,k)
        z = (im(log((1-wk/A) / (1-wk*A))) - pi*(1-t)) * 2/k
        return matrix([x, y, z]) * scale
    return patch

def trigen(*, k=2, t=0.5, cutoff=3, Nr=32, Ni=32, **others):
    b = cospi(t) / (1 + sinpi(t))
    t0 = arg((-1j-b) / (1 + b*1j))
    N1 = min(max(round(-t0 / pi * Nr), 1), Nr-1)

    def cm(z):
        y = (exp(z)+b) / (1+b*exp(z))
        y = (1j*y-1) / (1j-y)
        return root(y, k)

    yield from bilinear(t0*1j, 0, -cutoff+t0*1j, -cutoff, N1, Ni, cm)
    yield from bilinear(t0*1j, -pi*1j, -cutoff+t0*1j, -cutoff-pi*1j, Nr-N1, Ni, cm)

def syms(*, k=2, **others):
    return pointgroup("Dh", k)
