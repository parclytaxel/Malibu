"""
Data for do Carmo's "lemons" of constant Gaussian curvature.

k = elliptic modulus, should be positive
"""
from mpmath import cos, acsc, pi, ellipe, mpc, chop
from .revolution import prepatch as pp, trigen as tg

def prepatch(k, **others):
    curve = lambda t: mpc(k*cos(t), chop(ellipe(t, k**2)))
    return pp(curve=curve, **others)
def trigen(k, **others):
    L = acsc(max(k, 1))
    return tg(curve_a=-L, curve_b=L, **others)
