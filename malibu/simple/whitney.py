"""
Data for the Whitney umbrella.

The parametric equation is [uv, u, v^2] for u, v in [-S, S].

S = absolute bound on u and v, see above
N = resolution for u and v
"""
from mpmath import pi, matrix, rect
from malibu.trigens import bilinear

def prepatch(**others):
    def patch(w):
        u, v = w.real, w.imag
        return matrix([u*v, u, v*v])
    return patch

def trigen(*, S=1, N=32, **others):
    yield from bilinear(S*(-1-1j), S*(1-1j), S*(-1+1j), S*(1+1j), N, N)
