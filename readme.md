**Malibu** is a tool to generate STL files of 3-dimensional surfaces from a parametric description, with a view to easily modifying aspects of the surface by parameters.

This project began in [Dounreay](https://gitlab.com/parclytaxel/Dounreay), out of my desire to visualise minimal surfaces in Blender (after I needed the program to understand how the _New Horizons_ and _Curiosty_ probes looked like for my pony drawings). I implemented the equations for several such surfaces, including Alan Schoen's [gyroid](https://minimal.sitehost.iu.edu/archive/Triply/genus3/Gyroid%20small/web/index.html), and realised the generality of it all, but there were a few nagging inflexibilities in my hastily assembled driver code, leading to this repository.

The name comes from [Malibu Lagoon State Beach (Surfrider Beach)](https://en.wikipedia.org/wiki/Malibu_Lagoon_State_Beach), a world-renowned surfing destination in the eponymous town in California. The code in Dounreay used dynamic imports to get surface data, with the imported file called `surf`; this abbreviation is also seen in various places in Blender.

# The rendering system

Malibu depends on [mpmath](http://mpmath.org) and input files are strongly encouraged to make use of it; many interesting surfaces require special functions when expressed in parametric form, while the Weierstrass–Enneper parametrisation of minimal surfaces demands complex integration.

`render(prepatch, trigen, syms, params, outfn)` is Malibu's key function. Its arguments are

* `prepatch(**params)`: a function that takes in surface parameters as keyword arguments. It returns another function `patch(z)` that takes a complex number and returns a length-3 real mpmath vector representing the point in 3D space that `z` maps to. `patch()` in Dounreay took in the surface parameters alongside `z`, thus requiring recomputation of auxiliary numbers all the time and slowing the rendering process. As a further speedup, `patch()` is wrapped in a memoising function before being called, since the triangles from `trigen()` usually share vertices.
* `trigen(**params)`: takes in parameters and yields (not returns) 3-tuples of complex numbers representing triangles of the rendered surface. Malibu has a few convenience functions to generate structured triangle meshes. An early version of Dounreay relied on matplotlib to generate the requisite mesh from the points themselves; it was scrapped because the output was not predictable enough.
* `syms(**params)`: takes in parameters and returns an iterable of 4×4 affine transformation matrices (i.e. bottom row is 0, 0, 0, 1) that are applied to the surface resulting from calling `patch()` at the points yielded by `trigen()`. If not provided, only the identity transformation is applied. If not callable, it must be a sequence of transformation matrices. As with `trigen()`, Malibu has functions to generate the matrices corresponding to common symmetries.  
Uniquely among the three "big" arguments, the computed `patch()` function is one of the keyword arguments provided to `syms()`, under the identifier `patch`. This is particularly useful for periodic surfaces where the periods are not easy to compute directly from the parameters.
* `params`: a dictionary of all the parameters required by the previous three arguments.
* `outfn`: if not `None` (see below) the return value of the function is sent to `write_stl()` and written to this filename plus `.stl`; see below.

The output of `render()` is a list of 3-tuples (triangles) of 3D points. This can be passed to other functions, including `write_stl(tris, fn)` which writes the triangles to disk as an _ASCII_ STL file. Binary STL was not chosen because its definition is limited to 32-bit floating-point numbers, common when the standard was first defined but now mostly deprecated in favour of 64-bit floating point. (Blender's import utility for STL files keeps the precision in ASCII files just fine.)

`render_from()` is like `render()`, but replaces the first three arguments with a module or class having those three arguments. This is how the Dounreay `rendsurf()` worked, being passed a string corresponding to a module; the restrictions on module names led to the divorce of module data from function data in Malibu.

Example of rendering a torus:
```
from malibu import render_from
from malibu.simple import torus

render_from(torus, params={"R": 1, "r": 0.5}, outfn="torus")
```

For maximum convenience in extracting parameters (and assigning default values), `prepatch()/trigen()/syms()` can be declared as below:
```
def prepatch(*, k, bonnet=0, **others):
    # code here
```
