"""
Data for Schwarz's H (hexagonal) surface.

Equations from Sven Lidin (1988), Ring-like minimal surfaces,
Journal of Physics 49, 421-427. The paper uses E to parametrise the surfaces;
s = sqrt(1-E^2) is used here to cut down on square roots.

This surface is made out of "triangular catenoids". These motifs reach
their maximum aspect ratio at around s = 0.8, the default value for this parameter.
Scaling is performed to make its side length 2 and the overall structure
has space group P6_3/mmc (#194).

s = free parameter determining the unit cell height
N = grid resolution
pres = presentation of the unit cell. By default it is a five-ended pipe;
       "triangle" gives two triangular catenoids instead,
       "saddle" reveals the type 13 line group symmetry, the same as an order-3
       saddle tower (which the H surface converges to as s tends to 1).
"""
from mpmath import *
from malibu.trigens import sector
from malibu.symmetry.transforms import promote, mprod
from malibu.symmetry.generators import translate, permu
from malibu.symmetry import pointgroup, linegroup

def prepatch(*, s=0.8, **others):
    b = 2*(7*s*s-4) / (s+2)**2
    c = ((s-2) / (s+2))**2
    d = (s+1) / (s-1)
    def R(w):
        w2 = power(w, 2)
        return sqrt(((w2 + b)*w2 + c) * (w2 + d) * (w2 - 1))
    def patch(w):
        x = re(quad(lambda t: (1 - t*t) / R(t), [0, w]))
        y = im(quad(lambda t: (1 + t*t) / R(t), [0, w]))
        z = re(quad(lambda t: 2*t / R(t), [0, w]))
        return matrix([x, y, z])
    return patch

def trigen(*, s=0.8, N=16, **others):
    arc_c = 1j/sqrt(3)
    boun = arg(mpc(2*sqrt(1-s*s), sqrt(3)*s) / (2+s) + arc_c)
    rf = lambda th: rect(2/sqrt(3), th) - arc_c
    yield from sector(0, rf, pi/6, boun, N)
    yield from sector(0, rf, boun, pi/2, N)

def h_aspectratio(s):
    """Return the ratio of height to base side length for the
    H triangular catenoid (half of a unit cell) with given s."""
    pole = mpc(2*sqrt(1-s*s), sqrt(3)*s) / (2+s)
    v = prepatch(s=s)(pole)
    return 0.5 * v[0] / v[1]

def syms(*, s=0.8, pres=None, patch, **others):
    pole = mpc(2*sqrt(1-s*s), sqrt(3)*s) / (2+s)
    scale = 2*patch(pole)[1]
    qh = patch(1)[0] / scale
    dx = 2/sqrt(3) - patch(1)[2] / scale
    print("catenoid aspect ratio =", qh)
    print("unit cell height =", 4*qh)
    if pres == "triangle":
        return mprod(pointgroup("S", 2),
                     [translate(1/sqrt(3), 0, qh)],
                     pointgroup("Dh", 3),
                     [promote(permu(3, 2, -1) / scale, dx)])
    if pres == "saddle":
        return mprod(linegroup(13, 3, 4*qh),
                     [promote(permu(-3, 2, 1) / scale, 2/sqrt(3) - dx)])
    return mprod(pointgroup("Dh", 3),
                 pointgroup("S", 2, basis=translate(1/sqrt(12), 0.5, qh)),
                 [promote(permu(3, 2, -1) / scale, dx, 0, 2*qh)])
