"""
Data for the "Mylar balloon", the surface of revolution of most volume
given a fixed-length generating curve.

r = scale factor
"""
from mpmath import sqrt, cos, mpc, ellipe, ellipf, pi
from .revolution import prepatch as pp, trigen as tg

def prepatch(r=1, **others):
    def curve(t):
        return r * mpc(cos(t), ellipe(t, 0.5)*sqrt(2) - ellipf(t, 0.5)/sqrt(2))
    return pp(curve=curve, **others)
def trigen(**others):
    return tg(curve_a=-pi/2, curve_b=pi/2, **others)
