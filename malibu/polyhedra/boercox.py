"""
Data for the Boerdijk–Coxeter helix.

This demonstrates how linegroup() can handle arbitrary helices.

N = half number of triangles to render
"""
from mpmath import *
from malibu.symmetry import linegroup

h = 1 / sqrt(10)
theta = acos(-2/3)
r = 3 * sqrt(3) / 10

def prepatch(**others):
    def patch(w):
        z = rect(r, w*theta)
        return matrix([z.real, z.imag, w*h])
    return patch

def trigen(**others):
    yield (0, 1, 3)

def syms(*, N=32, **others):
    return linegroup(5, N, h, mpc(-2, sqrt(5)))
