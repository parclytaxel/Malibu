"""
Data for the Roman (or Steiner) surface.

If (x,y,z) is on the unit sphere, (yz,zx,xy) is a point on the surface.
Therefore we stereographically project a fundamental region; since
the surface has T_d symmetry and the whole sphere forms a double cover,
1/48th of the sphere forms a fundamental region.

N = grid resolution
"""
from mpmath import *
from malibu.trigens import sector
from malibu.symmetry import pointgroup

def prepatch(**others):
    def patch(w):
        wx, wy = w.real, w.imag
        denom = wx**2 + wy**2 + 1
        x = 2*wx / denom
        y = 2*wy / denom
        z = (1 - wx**2 - wy**2) / denom
        # Double size to fit nicely in Blender's default cube
        return 2 * matrix([y*z, z*x, x*y])
    return patch

def trigen(*, N=32, **others):
    arc = lambda th: rect(sqrt(2), th) - 1
    yield from sector(0, arc, 0, pi/12, N)

syms = tuple(pointgroup("Td"))
