"""
Data for axially symmetric polyhedra.
"""
from mpmath import *
from malibu.symmetry import pointgroup

class prism:
    """Data for an n-prism with n-gon edge length 1.

    n = number of sides
    h = prism height"""
    def prepatch(*, n, h=1, **others):
        R = 1 / (2*sin(pi/n))
        z = root(1, n, 1)
        pts = matrix([[0, 0, h/2],
                      [R, 0, h/2],
                      [R, 0, -h/2],
                      [R*z.real, R*z.imag, h/2]]).T
        def patch(w):
            return pts[:,w]
        return patch

    def trigen(**others):
        yield (0, 1, 3)
        yield (1, 2, 3)

    def syms(n, **others):
        return pointgroup("D", n)

class antiprism:
    """Data for an n-antiprism with n-gon edge length 1.

    n = number of sides
    s = length of edges connecting the two n-gons"""
    def prepatch(*, n, s=1, **others):
        R = 1 / (2*sin(pi/n))
        Rp = rect(R, pi/n)
        k = 1 / (2*cos(pi/(2*n)))
        h = sqrt(s**2 - k**2)
        pts = matrix([[0, 0, -h/2],
                      [Rp.real, -Rp.imag, -h/2],
                      [R, 0, h/2],
                      [Rp.real, Rp.imag, -h/2]]).T
        def patch(w):
            return pts[:,w]
        return patch

    def trigen(**others):
        yield (0, 1, 3)
        yield (1, 2, 3)

    def syms(n, **others):
        return pointgroup("S", 2*n)

class pyramid:
    """Data for an n-(bi)pyramid with n-gon edge length 1.

    n = number of sides
    bi = if True, construct a bipyramid
    h = distance from apex to xy-plane"""
    def prepatch(*, n, h, **others):
        R = 1 / (2*sin(pi/n))
        z = root(1, n, 1)
        pts = matrix([[0, 0, h],
                      [R, 0, 0],
                      [R*z.real, R*z.imag, 0]]).T
        def patch(w):
            return pts[:,w]
        return patch

    def trigen(**others):
        yield (0, 1, 2)

    def syms(n, bi=False, **others):
        return pointgroup("Ch" if bi else "C", n)

class trapezohedron:
    """Data for an n-trapezohedron where the middle vertices
    lie on two rings of radius 1.

    n = number of sides
    h = half-distance between the rings
    
    To get a rectangular 2n-sided die, set
    h = sqrt(1 / (cot(pi/(2*n))**4-1))."""
    def prepatch(*, n, h, **others):
        L = h / tan(pi/(2*n))**2
        z = root(1, 2*n, 1)
        pts = matrix([[0, 0, L],
                      [1, 0, -h],
                      [z.real, z.imag, h]]).T
        def patch(w):
            return pts[:,w]
        return patch

    def trigen(**others):
        yield (0, 1, 2)

    def syms(n, **others):
        return pointgroup("Dd", n)
