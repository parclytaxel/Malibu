"""
Data for a plain torus.

R, r = major and minor radii of torus
NR = resolution in toroidal (around symmetry axis) direction
Nr = resolution in poloidal (around tube) direction
"""
from mpmath import pi, matrix, rect
from malibu.trigens import bilinear

def prepatch(*, R, r, **others):
    def patch(w):
        phi, t = w.real, w.imag
        a = R + rect(r, t)
        b = rect(a.real, phi)
        return matrix([b.real, b.imag, a.imag])
    return patch

def trigen(*, NR=60, Nr=30, **others):
    yield from bilinear(0, 2*pi, 2j*pi, (2+2j)*pi, NR, Nr)
