"""
Data for the Platonic and Archimedean solids and their duals.

As they do not have any free parameters other than the trivial
location/rotation/scale, the only argument that needs to be passed
is the name of the solid. Conway polyhedron notation is used for
canonical identifiers.

kind = identifier string
"""
import re as rgx
from mpmath import *
from malibu.symmetry import pointgroup
nonaz_re = rgx.compile(r"[^a-z]")

tc1 = sqrt(2) - 1
tc2 = (sqrt(2) + 1) / 3
ec1 = sqrt(2) + 1
ec2 = sqrt(2) / 3 + 1
bc1 = 2*sqrt(2) + 1
trib = polyroots([1,-1,-1,-1])[0]

fee = phi - 1

lut = {"t": (matrix([[-1, 1, 1], [1, -1, 1], [1, 1, -1]]),
             [(0, 1, 2)], ["S", 4]),
       "tetrahedron": "t",
       "c": (matrix([[-1, 1, 1], [1, -1, 1], [1, 1, 1]]),
             [(0, 1, 2)], ["T"]),
       "cube": "c",
       "o": (eye(3),
             [(0, 1, 2)], ["Dh", 2]),
       "octahedron": "o",
       "d": (matrix([[phi, 0, fee], [1, 1, 1], [(2+phi)/5, 0, (3*phi+1)/5]]),
             [(0, 1, 2)], ["I"]),
       "dodecahedron": "d",
       "i": (matrix([[phi, 1, 0], [1, 0, phi], [(2*phi+1)/3, 0, phi/3]]),
             [(0, 1, 2)], ["I"]),
       "icosahedron": "i",

       "tt": (matrix([[1, 1, 3], [3, 1, 1], [5/3, 5/3, 5/3],
                      [1, -1, 1], [-1, -1, 3]]),
              [(0, 1, 2), (0, 3, 1), (0, 4, 3)], ["T"]),
       "truncatedtetrahedron": "tt",
       "tc": (matrix([[1, tc1, 1], [tc2, tc2, tc2], [tc1, 1, 1],
                      [0, 0, 1], [1, -tc1, 1]]),
              [(0, 1, 2), (0, 2, 3), (0, 3, 4)], ["O"]),
       "truncatedcube": "tc",
       "to": (matrix([[1, 0, 2], [2, 0, 1], [1, 1, 1],
                      [0, 1, 2], [0, 0, 2]]),
              [(0, 1, 2), (0, 2, 3), (0, 3, 4)], ["O"]),
       "truncatedoctahedron": "to",
       "ac": (matrix([[1, 0, 1], [2/3, 2/3, 2/3], [0, 1, 1],
                      [0, 0, 1]]),
              [(0, 1, 2), (0, 2, 3)], ["O"]),
       "cuboctahedron": "ac",
       "ec": (matrix([[1, 1, ec1], [0, 0, ec1], [1, -1, ec1],
                      [ec1, 1, 1], [ec2, ec2, ec2]]),
              [(0, 1, 2), (0, 2, 3), (0, 3, 4)], ["O"]),
       "rhombicuboctahedron": "ec",
       "bc": (matrix([[ec1, 1, bc1], [0, 0, bc1], [ec1, -1, bc1],
                      [bc1, 1, ec1], [ec1, ec1, ec1], [1, ec1, bc1]]),
              [(0, 1, 2), (0, 2, 3), (0, 3, 4), (0, 4, 5), (0, 5, 1)], ["O"]),
       "truncatedcuboctahedron": "bc",
       "sc": (matrix([[trib, 1, 1/trib], [trib**2/3, trib**2/3, trib**2/3],
                      [1, 1/trib, trib], [trib, -1/trib, 1], [trib, 0, 0]]),
              [(0, 1, 2), (0, 2, 3), (0, 3, 4)], ["O"]),
       "snubcube": "sc",
       "td": (matrix([[2*phi, fee, phi],
                      [phi+1, phi, 2],
                      [phi, 0, phi+1],
                      [2*phi, -fee, phi],
                      [(5*phi+2)/3, 0, phi-1/3]]),
              [(0, 1, 2), (0, 2, 3), (0, 3, 4)], ["I"]),
       "truncateddodecahedron": "td",
       "ti": (matrix([[2+phi, 1, 2*phi],
                      [(2*phi+9)/5, 0, (11*phi+2)/5],
                      [2+phi, -1, 2*phi],
                      [2*phi+1, 0, phi],
                      [2*phi+1, 2, phi]]),
              [(0, 1, 2), (0, 2, 3), (0, 3, 4)], ["I"]),
       "truncatedicosahedron": "ti",
       "ad": (matrix([[(phi+1)/2, 0.5, phi/2], [(2+phi)/5, 0, (3*phi+1)/5],
                      [(phi+1)/2, -0.5, phi/2], [(2*phi+1)/3, 0, phi/3]]),
              [(0, 1, 2), (0, 2, 3)], ["I"]),
       "icosidodecahedron": "ad",
       "ed": (matrix([[2+phi, 0, 1+phi],
                      [(5*phi+4)/3, 0, 1+phi/3],
                      [2*phi+1, 1, 1],
                      [1+phi, phi, 2*phi],
                      [(3*phi+6)/5, 0, (9*phi+3)/5]]),
              [(0, 1, 2), (0, 2, 3), (0, 3, 4)], ["I"]),
       "rhombicosidodecahedron": "ed",
       "bd": (matrix([[3*phi-1, fee, phi+1],
                      [2*phi, phi, 3],
                      [2*phi-1, 0, phi+2],
                      [3*phi-1, -fee, phi+1],
                      [2*phi+1, 0, phi],
                      [2*phi+1, 2*fee, phi]]),
              [(0, 1, 2), (0, 2, 3), (0, 3, 4), (0, 4, 5), (0, 5, 1)], ["I"]),
       "truncatedicosidodecahedron": "bd",

       "kt": (matrix([[-1, 1, 1], [1, -1, 1], [3/5, 3/5, 3/5]]),
              [(0, 1, 2)], ["T"]),
       "triakistetrahedron": "kt",
       "ko": (matrix([[1, 0, 0], [0, 1, 0], [tc1, tc1, tc1]]),
              [(0, 1, 2)], ["O"]),
       "triakisoctahedron": "ko",
       "kc": (matrix([[1, 1, 1], [0, 0, 1.5], [1, -1, 1]]),
              [(0, 1, 2)], ["O"]),
       "tetrakishexahedron": "kc",
       "jc": (matrix([[1, 1, 1], [0, 0, 2], [1, -1, 1]]),
              [(0, 1, 2)], ["O"]),
       "rhombicdodecahedron": "jc",
       "oc": (matrix([[0, 0, 1], [sqrt(0.5), 0, sqrt(0.5)], [bc1/7, bc1/7, bc1/7]]),
              [(0, 1, 2)], ["Oh"]),
       "deltoidalicositetrahedron": "oc",
       "mc": (matrix([[1, 0, 1],
                      [(4-sqrt(2))/3, (4-sqrt(2))/3, (4-sqrt(2))/3],
                      [0, 0, (sqrt(2)+10)/7]]),
              [(0, 1, 2)], ["Oh"]),
       "disdyakisdodecahedron": "mc",
       "gc": (matrix([[0, 0, trib**2],
                      [trib, 1/trib, 2+1/trib],
                      [trib, trib, trib],
                      [1/trib, 2+1/trib, trib],
                      [-1/trib, trib, 2+1/trib]]),
              [(0, 1, 2), (0, 2, 3), (0, 3, 4)], ["O"]),
       "pentagonalicositetrahedron": "gc",
       "ki": (matrix([[phi, 1, 0], [1, 0, phi],
                      [(7*phi-1)/11, (7*phi-1)/11, (7*phi-1)/11]]),
              [(0, 1, 2)], ["I"]),
       "triakisicosahedron": "ki",
       "kd": (matrix([[phi, 0, fee], [1, 1, 1], [(3*phi+12)/19, 0, (15*phi+3)/19]]),
              [(0, 1, 2)], ["I"]),
       "pentakisdodecahedron": "kd",
       "jd": (matrix([[phi, 0, fee], [1, 1, 1], [1, 0, phi]]),
              [(0, 1, 2)], ["I"]),
       "rhombictriacontahedron": "jd",
       "od": (matrix([[0, 0, 1],
                      [phi/3, 0, (phi+1)/3],
                      [0, (3*phi-1)/11, (5*phi+2)/11]]),
              [(0, 1, 2)], ["Ih"]),
       "deltoidalhexecontahedron": "od",
       "md": (matrix([[0, 0, 1],
                      [(3*phi-2)/5, 0, (phi+3)/5],
                      [0, (5*phi-7)/3, (3*phi-2)/3]]),
              [(0, 1, 2)], ["Ih"]),
       "disdyakistriacontahedron": "md"}

def lookup(key):
    key = nonaz_re.sub("", key.lower())
    while type(key) == str:
        key = lut[key]
    return key

def prepatch(*, kind, **others):
    pts = lookup(kind)[0].T
    def patch(w):
        return pts[:,w]
    return patch

def trigen(*, kind, **others):
    yield from lookup(kind)[1]

def syms(*, kind, **others):
    return pointgroup(*lookup(kind)[2])
