from functools import lru_cache
from mpmath import matrix, norm

def render(prepatch, trigen, syms=None, params={}, outfn=None):
    """Render a surface into a list of 3D triangles."""
    patch = lru_cache(None)(prepatch(**params))
    tris = [[patch(w) for w in tri] for tri in trigen(**params)]

    try:
        params["patch"] = patch
        symmats = syms(**params)
    except TypeError:
        symmats = syms
    if symmats is not None:
        tris2 = []
        for M in symmats:
            Mtris = [[M[:3,:3] * w + M[:3,3] for w in tri] for tri in tris]
            tris2.extend(Mtris)
        tris = tris2

    if outfn is not None:
        write_stl(tris, outfn)
    return tris

def write_stl(tris, fn):
    """Writes the given triangle list in ASCII STL format to the given filename."""
    f = open(f"{fn}.stl", 'w')
    f.write(f"solid {fn}\n")
    for (a, b, c) in tris:
        v = c - b
        w = a - b
        nx = v[1]*w[2] - v[2]*w[1]
        ny = v[2]*w[0] - v[0]*w[2]
        nz = v[0]*w[1] - v[1]*w[0]
        try:
            normal = matrix([nx, ny, nz])
            normal /= norm(normal)
        except ZeroDivisionError:
            normal = matrix([1, 0, 0])
        f.write(f"facet normal {float(normal[0])} {float(normal[1])} {float(normal[2])}\n")
        f.write( "outer loop\n")
        f.write(f"vertex {float(a[0])} {float(a[1])} {float(a[2])}\n")
        f.write(f"vertex {float(b[0])} {float(b[1])} {float(b[2])}\n")
        f.write(f"vertex {float(c[0])} {float(c[1])} {float(c[2])}\n")
        f.write( "endloop\nendfacet\n")
    f.write(f"endsolid {fn}\n")
    f.close()

def render_from(desc, params={}, outfn=None):
    """desc is a module or class containing prepatch(), trigen()
    and optionally syms. This is a convenience function to render
    the described surface."""
    syms = getattr(desc, "syms", None)
    return render(desc.prepatch, desc.trigen, syms, params, outfn)
