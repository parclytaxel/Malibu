"""
Data for saddle surfaces.

The simplest formula is z = Re(w^k). k = 2 gives a classic saddle
and k = 3 the so-called monkey saddle.

k = number of depressions
R = grid radius
N = grid resolution
"""
from mpmath import *
from malibu.trigens import sector
from malibu.symmetry import pointgroup

def prepatch(*, k=2, **others):
    def patch(w):
        return matrix([w.real, w.imag, (w**k).real])
    return patch

def trigen(*, k=2, R=1.2, N=32, **others):
    arc = lambda th: rect(R, th)
    yield from sector(0, arc, 0, pi/(2*k), N)

def syms(*, k=2, **others):
    return pointgroup("Dd", k)
