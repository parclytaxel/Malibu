"""
Data for the Scherk surfaces.

The grid [-cutoff, 0]×[0, pi/2] is conformally transformed
to an appropriate parametric input, the unit sector from 0 to pi/4
punctured at 1.

bonnet = Bonnet angle (0 or "1" gives first surface,
                       pi/2 or "2" gives second surface)
cutoff = width of grid before conformal transformation, see above
Nr = grid resolution in real direction
Ni = grid resolution in imaginary direction
"""
from mpmath import *
from malibu.trigens import bilinear
from malibu.symmetry import pointgroup

def prepatch(*, bonnet, **others):
    if bonnet == "1":
        bonnet_v = 1
    elif bonnet == "2":
        bonnet_v = 1j
    else:
        bonnet_v = expj(bonnet)
    def patch(w):
        x = re(bonnet_v * atan(w))
        y = im(bonnet_v * atanh(w))
        z = re(bonnet_v * atanh(w*w))
        return matrix([x, y, z]) * 4/pi
    return patch

def trigen(*, cutoff=3, Nr=16, Ni=32, **others):
    cm1 = lambda z: sqrt((1j-exp(z)) / (1j+exp(z)))
    cm2 = lambda z: cm1(z).conjugate() * 1j
    yield from bilinear(0, -pi/2j, -cutoff, -cutoff-pi/2j, Nr, Ni, cm1)
    yield from bilinear(0, -pi/2j, -cutoff, -cutoff-pi/2j, Nr, Ni, cm2)

syms = tuple(pointgroup("S", 4))
