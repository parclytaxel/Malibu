#!/usr/bin/env python3
# A showcase of what Malibu can currently do.
# After the surfaces of interest are rendered,
# open the STL files in Blender and see if anything looks amiss.
from mpmath import pi
from malibu import render_from
from malibu.simple import torus, whitney, saddle, oloid
from malibu.polyhedra.axial import prism, antiprism, pyramid, trapezohedron
from malibu.polyhedra import uniform, boercox
from malibu.revolution import torus2, pseudosphere, gabriel, albuminflask, docarmolemon, unduloid, mylar
from malibu.minimal.classic import helicat, scherk, enneper, bour, riemann
from malibu.minimal.bjorling import henneberg, catalan, meeksbius
from malibu.minimal.triperiod import schwarzt, schwarzclp, schwarzh
from malibu.minimal import richmond, knoid, saddletower, saddletower2, chengack, costa
from malibu.other import boy, roman

### Simple surfaces

# Torus
"""
render_from(torus, {"R": 1, "r": 0.5}, "torus")
render_from(torus, {"R": 1, "r": 1}, "horn_torus")
render_from(torus, {"R": 1, "r": 1.5}, "spindle_torus")
"""

# Whitney umbrella
"""render_from(whitney, outfn="whitney")"""

# Saddles
"""
render_from(saddle, {"k": 2}, outfn="saddle")
render_from(saddle, {"k": 3}, outfn="monkey_saddle")
render_from(saddle, {"k": 8}, outfn="octopus_saddle")
"""

# Oloid
"""render_from(oloid, outfn="oloid")"""

### Polyhedra

# Axially symmetric
"""
render_from(prism, {"n": 5}, "pentagonal_prism")
render_from(prism, {"n": 7, "h": 0.75}, "short_heptagonal_prism")
render_from(prism, {"n": 4}, "cube")
render_from(antiprism, {"n": 5}, "pentagonal_antiprism")
render_from(antiprism, {"n": 4, "s": 5}, "tall_square_antiprism")
render_from(antiprism, {"n": 3}, "octahedron")
render_from(pyramid, {"n": 7, "h": 1}, "heptagonal_pyramid")
render_from(pyramid, {"n": 6, "bi": True, "h": 1}, "hexagonal_bipyramid")
render_from(trapezohedron, {"n": 5, "h": 0.10616611026445434}, "pentagonal_trapezohedron")
"""

# Platonic/Archimedean/Catalan solids
"""
render_from(uniform, {"kind": "T"}, "tetrahedron")
render_from(uniform, {"kind": "C"}, "cube")
render_from(uniform, {"kind": "O"}, "octahedron")
render_from(uniform, {"kind": "D"}, "dodecahedron")
render_from(uniform, {"kind": "I"}, "icosahedron")

render_from(uniform, {"kind": "tT"}, "truncated_tetrahedron")
render_from(uniform, {"kind": "tC"}, "truncated_cube")
render_from(uniform, {"kind": "tO"}, "truncated_octahedron")
render_from(uniform, {"kind": "aC"}, "cuboctahedron")
render_from(uniform, {"kind": "eC"}, "rhombicuboctahedron")
render_from(uniform, {"kind": "bC"}, "truncated_cuboctahedron")
render_from(uniform, {"kind": "sC"}, "snub_cube")
render_from(uniform, {"kind": "tD"}, "truncated_dodecahedron")
render_from(uniform, {"kind": "tI"}, "truncated_icosahedron")
render_from(uniform, {"kind": "aD"}, "icosidodecahedron")
render_from(uniform, {"kind": "eD"}, "rhombicosidodecahedron")
render_from(uniform, {"kind": "bD"}, "truncated_icosidodecahedron")

render_from(uniform, {"kind": "kT"}, "triakis_tetrahedron")
render_from(uniform, {"kind": "kO"}, "triakis_octahedron")
render_from(uniform, {"kind": "kC"}, "tetrakis_hexahedron")
render_from(uniform, {"kind": "jC"}, "rhombic_dodecahedron")
render_from(uniform, {"kind": "oC"}, "deltoidal_icositetrahedron")
render_from(uniform, {"kind": "mC"}, "disdyakis_dodecahedron")
render_from(uniform, {"kind": "gC"}, "pentagonal_icositetrahedron")
render_from(uniform, {"kind": "kI"}, "triakis_icosahedron")
render_from(uniform, {"kind": "kD"}, "pentakis_dodecahedron")
render_from(uniform, {"kind": "jD"}, "rhombic_triacontahedron")
render_from(uniform, {"kind": "oD"}, "deltoidal_hexecontahedron")
render_from(uniform, {"kind": "mD"}, "disdyakis_triacontahedron")
"""

# Boerdijk–Coxeter helix
"""render_from(boercox, outfn="boerdijk-coxeter_helix")"""

### Surfaces of revolution

# Torus (explicit surface of revolution)
"""render_from(torus2, {"R": 1, "r": 0.1}, "torus")"""

# Pseudosphere
"""render_from(pseudosphere, {"curve_a": 0, "curve_b": 3}, "pseudosphere")"""

# Gabriel's Horn
"""render_from(gabriel, {"curve_a": 1, "curve_b": 8}, "gabriel")"""

# Parcly Taxel's Albumin Flask
"""render_from(albuminflask, outfn="albumin_flask")"""

# do Carmo's "lemons"
"""
render_from(docarmolemon, {"k": 0.7}, "lemon")
render_from(docarmolemon, {"k": 1.5}, "stadium")
"""

# Unduloid
"""
render_from(docarmolemon, {"k": 1.5}, "lemon")
render_from(unduloid, {"a": 1.7, "b": 1.2}, "unduloid")
"""

# Mylar balloon
render_from(mylar, outfn="mylar")

### Minimal surfaces

# Helicoid/catenoid associate family
"""
render_from(helicat, {"R": 1.2, "bonnet": 0}, "catenoid")
render_from(helicat, {"R": 1.2, "bonnet": pi/3}, "helicatenoid")
render_from(helicat, {"R": 1.2, "bonnet": pi/2}, "helicoid")
"""

# Scherk surface
"""
render_from(scherk, {"bonnet": "1"}, "scherk_first")
render_from(scherk, {"bonnet": "2"}, "scherk_second")
render_from(scherk, {"bonnet": pi/4}, "scherk_middle")
"""

# Enneper surface
"""
render_from(enneper, outfn="enneper")
render_from(enneper, {"k": 5, "R": 1.2}, "enneper5")
"""

# Bour's minimal surface
"""render_from(bour, outfn="bour")"""

# Riemann's minimal surface
"""render_from(riemann, {"sigma": 0.5}, "riemann")"""

# Henneberg surface
"""
render_from(henneberg, outfn="henneberg")
render_from(henneberg, {"cutoff": 2, "Ni": 64, "show_neile": True}, "neile_parabola")
"""

# Catalan's minimal surface
"""
render_from(catalan, outfn="catalan")
render_from(catalan, {"show_cycloid": True}, "cycloid")
"""

# Meeks's minimal Möbius strip
"""render_from(meeksbius, outfn="meeks_möbius_strip")"""

# Richmond surface
"""
render_from(richmond, outfn="richmond")
render_from(richmond, {"k": 4, "cutoff_r": 0.1}, "richmond4")
"""

# k-noid
"""
render_from(knoid, outfn="trinoid")
render_from(knoid, {"k": 7, "cutoff": 1.5}, "heptanoid")
"""

# Saddle tower
"""
render_from(saddletower, outfn="trisaddle")
render_from(saddletower, {"k": 4}, "tetrasaddle")
render_from(saddletower2, {"t": 0.7}, "bent_scherk")
render_from(saddletower2, {"k": 3, "t": 0.7}, "bent_trisaddle")
render_from(saddletower2, {"k": 3, "t": 0.2}, "bent_trisaddle2")
"""

# Chen–Gackstatter surface (M_1,1)
"""render_from(chengack, outfn="chen-gackstatter")"""

# Costa's minimal surface
"""render_from(costa, outfn="costa")"""

# Schwarz T/tP surface family and the gyroid
"""
render_from(schwarzt, outfn="schwarz_d")
render_from(schwarzt, {"lam": -150}, "schwarz_t")
render_from(schwarzt, {"bonnet": "P"}, "schwarz_p")
render_from(schwarzt, {"lam": -4, "bonnet": "P"}, "schwarz_pt")
render_from(schwarzt, {"lam": -100, "bonnet": "P", "pres": "square"}, "square_catenoid")
render_from(schwarzt, {"bonnet": "G"}, "gyroid")
"""

# Schwarz CLP surface
"""
render_from(schwarzclp, outfn="CLP")
render_from(schwarzclp, {"lam": 1.9}, "CLP2")
"""

# Schwarz H surface
"""
render_from(schwarzh, outfn="schwarz_h")
render_from(schwarzh, {"s": 0.4, "pres": "triangle"}, outfn="triangular_catenoid")
render_from(schwarzh, {"s": 0.999, "pres": "saddle"}, outfn="saddle_tower_cell")
"""

### Other surfaces

# Boy's surface
"""render_from(boy, outfn="boy_surface")"""

# Roman surface
"""render_from(roman, outfn="roman_surface")"""
