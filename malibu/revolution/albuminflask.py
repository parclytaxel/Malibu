"""
Data for the Albumin Flask (Parcly Taxel's genie bottle).

Canonically this is a volumetric flask, a bottle with a very long neck
and a teardrop-shaped base. The generating path for the implementation here is
M 0,0 C 82.2,0 108.1,71.8 10.5,148.7 V 433.3
"""
from mpmath import mpc
from .revolution import prepatch as pp, trigen as tg

def prepatch(**others):
    def path(t):
        if 0 <= t <= 1:
            p1 = (1-t) * 0 + t * 82.2
            p2 = (1-t) * 82.2 + t * (108.1+71.8j)
            p3 = (1-t) * (108.1+71.8j) + t * (10.5+148.7j)
            p4 = (1-t) * p1 + t * p2
            p5 = (1-t) * p2 + t * p3
            res = (1-t) * p4 + t * p5
        if 1 <= t <= 2:
            res = mpc(10.5, (2-t) * 148.7 + (t-1) * 433.3)
        return res / 100 # scale to a reasonable size
    return pp(curve=path, **others)

def trigen(**others):
    return tg(curve_a=0, curve_b=2, **others)
