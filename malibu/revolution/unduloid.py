"""
Data for the unduloid. The generating curve is formed by rolling
an ellipse along a line and tracing the focus.

a, b = ellipse semi-axes (a > b > 0)
"""
from mpmath import sqrt, hypot, sin, cos, mpf, mpc, ellipe, pi
from .revolution import prepatch as pp, trigen as tg

def prepatch(a=2, b=1, **others):
    a, b = mpf(a), mpf(b)
    d = sqrt((a+b)*(a-b))
    m = 1 - (a/b)**2
    def curve(t):
        F = (a-d*cos(t)) / hypot(a*sin(t), b*cos(t))
        return mpc(b*F, b*ellipe(t, m) - d*F*sin(t))
    return pp(curve=curve, **others)
def trigen(**others):
    return tg(curve_a=0, curve_b=2*pi, **others)
