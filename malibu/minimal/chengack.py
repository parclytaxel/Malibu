"""
Data for the Chen–Gackstatter (M_1,1) surface.

Equations from https://tpfto.wordpress.com/2019/03/28/the-chen-gackstatter-surface

cutoff = width of grid before conformal transformation
Nr = grid resolution in real direction
Ni = grid resolution in imaginary direction
"""
from mpmath import *
from malibu.trigens import bilinear
from malibu.symmetry import pointgroup

def prepatch(**others):
    e1 = gamma(0.25)**4 / (8*pi)
    f1 = gamma(0.25)**2 / (2*sqrt(pi))
    pig = 16*pi**3 / gamma(0.25)**8
    z_scale = sqrt(6*pig)

    def weierp_costa(z):
        return e1 * chop(2 / ellipfun("sn", f1*z, 0.5)**2 - 1)

    def weierz_costa(z):
        return pi*z + diff(lambda u: log(jtheta(1, pi*u, exp(-pi))), z)

    def patch(w):
        Z = weierz_costa(w)
        D = pi*w - pig*diff(weierp_costa, w)
        x = re(D - Z)
        y = im(D + Z)
        z = z_scale * re(weierp_costa(w))
        return matrix([x, y, z])
    return patch

def trigen(*, cutoff=0.9, Nr=24, Ni=24, **others):
    def cm(z):
        scale = 1 - log(cos(z.imag)) / cutoff
        return exp(mpc(z.real * scale - cutoff, z.imag)) / 2
    yield from bilinear(-pi/4j, 0, cutoff-pi/4j, cutoff, Nr, Ni, cm)

syms = tuple(pointgroup("Dd", 2))
