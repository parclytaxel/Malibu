"""
Point groups in three dimensions, including non-crystallographic groups.
"""
from .transforms import mprod, changebase_deco
from .generators import cyclic_z
from .constants import flip_z, flip_y, turn_x, swap_xy, cycle_axes, cycle_pent

@changebase_deco
def pointgroup(kind, n=1):
    """Yield matrices describing the specified point group.
    
    kind contains the letters in the group's Schoenflies notation and is
    case-sensitive, n is the index. Use basis=B to change the setting;
    see symdocs/pointgroups.md for a description of the default setting."""
    if kind == "C":
        yield from cyclic_z(n)
    elif kind == "S": # C_i = S_2
        if n % 2 == 1:
            yield from pointgroup("Ch", n)
        else:
            yield from cyclic_z(n, -1)
    elif kind == "Ch": # C_s = C_1h
        yield from mprod(cyclic_z(n), flip_z)
    elif kind == "Cv":
        yield from mprod(cyclic_z(n), flip_y)
    elif kind == "D":
        yield from mprod(cyclic_z(n), turn_x)
    elif kind == "Dd":
        yield from mprod(cyclic_z(2*n, -1), flip_y)
    elif kind == "Dh":
        yield from mprod(cyclic_z(n), flip_y, flip_z)

    elif kind == "T":
        yield from mprod(cycle_axes, pointgroup("D", 2))
    elif kind == "Td":
        yield from mprod(swap_xy, cycle_axes, pointgroup("D", 2))
    elif kind == "Th":
        yield from mprod(cycle_axes, pointgroup("Dh", 2))
    elif kind == "O":
        yield from mprod(cycle_axes, pointgroup("D", 4))
    elif kind == "Oh":
        yield from mprod(cycle_axes, pointgroup("Dh", 4))
    elif kind == "I":
        yield from mprod(cycle_pent, pointgroup("T"))
    elif kind == "Ih":
        yield from mprod(cycle_pent, pointgroup("Th"))
