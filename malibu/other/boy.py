"""
Data for Boy's surface.

This is the Bryant–Kusner parametrisation which minimises Willmore energy.
It is obtained by inverting a certain minimal surface.

N = grid resolution
"""
from mpmath import *
from malibu.trigens import sector
from malibu.symmetry import pointgroup

def prepatch(**others):
    def patch(w):
        w3, w4 = power(w,3), power(w,4)
        denom = polyval([1, sqrt(5), -1], w3)
        x = 1.5 * im(w * (1-w4) / denom)
        y = 1.5 * re(w * (1+w4) / denom)
        z = im((1 + w3**2) / denom) - 0.5
        v = matrix([x, y, z])
        return v / norm(v)**2
    return patch

def trigen(*, N=32, **others):
    arc = lambda th: rect(1, th)
    yield from sector(0, arc, 0, pi/3, N)
    yield from sector(0, arc, pi/3, 2*pi/3, N)

syms = tuple(pointgroup("C", 3))
