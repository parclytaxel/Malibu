"""
Data for Riemann's minimal surface.

Equations from Martín and Pérez, "Superficies minimales foliadas
por circunferencias: los ejemplos de Riemann",
https://wpd.ugr.es/~jperez/wordpress/wp-content/uploads/riemann.pdf

sigma = free parameter controlling tilt of the catenoid-like bridges (positive)
cutoff = width of grid before conformal transformation
Nr = grid resolution in real direction
Ni = grid resolution in imaginary direction
"""
from mpmath import *
from malibu.trigens import bilinear
from malibu.symmetry import pointgroup

def prepatch(*, sigma=1, **others):
    m = 1 / (1+sigma)
    m_ = sigma / (1+sigma) # 1-m or m'
    factor = (1+sigma) / sqrt(sigma)
    def patch(w):
        ephi = asin(sqrt(1-w))
        E = ellipe(ephi, m)
        F = ellipf(ephi, m)
        P = sqrt( (sigma+w)*(1-w) / (w*(1+sigma)) )
        Q = 2*(m_*F - E) + P
        x = factor * im(Q)
        y = factor * re(P)
        z = 2 * im(F)
        return matrix([x, y, z])
    return patch

def trigen(*, sigma=1, cutoff=3, Nr=32, Ni=32, **others):
    if sigma == 1:
        yield from bilinear(0, -pi/1j, -cutoff, -cutoff-pi/1j, Nr, Ni, exp)
        return

    def yf(x):
        L = (1-x**2) / ((x+1)*sigma**2 - x + 1)
        return (1-sigma)*L + sqrt(2*L)
    def inverse_yf(k, sgn):
        b1 = k*(1-sigma) + 1
        s1 = sqrt(2*k*(1-sigma) + 1)
        L = (b1 + sgn*s1) / (1-sigma)**2
        b2 = L*(1+sigma)*(1-sigma)
        p1 = L*(1+sigma**2)-2
        p2 = 2*L*sigma
        s2 = sqrt((p1+p2)*(p1-p2))
        return (b2-s2) / 2
    theta_mid = acos((1-sigma) / (1+sigma))
    theta_hump = acos(  (1+sigma-sqrt(3*(sigma-3)*(3*sigma-1))) / (4*(1-sigma))  )
    Ni += Ni & 1
    hNi = Ni // 2
    angles = linspace(0, theta_mid, hNi+1)
    for theta in angles[hNi-1:0:-1]:
        k = yf(cos(theta))
        sgn = -1 if sigma <= 3 or theta <= theta_hump else +1
        angles.append(acos(inverse_yf(k, sgn)))
    angles.append(pi)

    def cm(z):
        z = mpc(z.real, angles[round(z.imag)])
        return 2*sigma*exp(z) / ((sigma-1)*exp(z) + sigma+1)
    yield from bilinear(0, Ni*1j, -cutoff, -cutoff+Ni*1j, Nr, Ni, cm)

def syms(*, sigma=1, **others):
    m_ = sigma / (1+sigma)
    x = 2 / sqrt(sigma) * ((1+sigma)*ellipe(m_) - ellipk(m_))
    z = 2 * ellipk(m_)
    print(f"translation vector: x = {x}, z = {z}")
    return pointgroup("Dd", 1)
