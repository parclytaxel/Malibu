"""
Data for the Henneberg surface, a non-orientable minimal surface.

The Weierstrass–Enneper parametrisation has
f(z) = 1-1/z^4, g(z) = z

cutoff = width of grid before exponential map
Nr = grid resolution in real direction
Ni = grid resolution in imaginary direction
show_neile = if True, render just a quarter of the surface to show
             Neile's semicubical parabola
"""
from mpmath import *
from malibu.trigens import bilinear
from malibu.symmetry.generators import permu
from malibu.symmetry import pointgroup

def prepatch(**others):
    def patch(w):
        if w == 0:
            return matrix([0, 0, 0])
        A = w + 1/(3*w**3)
        B = 1/w + w**3/3
        x = re(A - B)
        y = im(A + B)
        z = re(w**2 + 1/w**2)
        return matrix([x, y, z])
    return patch

def trigen(*, cutoff=0.5, Nr=32, Ni=24, **others):
    yield from bilinear(0, -pi/4j, -cutoff, -cutoff-pi/4j, Nr, Ni, exp)

def syms(show_neile=False, **others):
    return pointgroup("Cv", 1, basis=permu(2, -1, 3)) if show_neile else pointgroup("Dd", 2)
