"""
Data for the Schwarz T surface family.

The Weierstrass–Enneper parametrisation has
g(w) = w, f(w) = 1/sqrt(w^8 + lam*w^4 + 1)
where lam is in (-inf, -2); lam = -2 gives Scherk's first surface.
This family includes Schwarz's classical cubic D/G/P surfaces
at lam = -14 and is named for being a tetragonal distortion
of those surfaces.

The T surface has space group P4_2/nnm (#134), the cubic D surface Pn-3m (#224).
Tetragonal P has space group I4/mmm (#139), cubic P Im-3m (#229).
Alan Schoen's gyroid (G surface) has space group I4_132 (#214).

Unit cells are scaled to have a square base of side length 2.
syms() prints out the unit cell's height and aspect ratio by means of
a function which can be imported by itself.

Equations from Cvijović and Klinowski's papers:
https://hal.archives-ouvertes.fr/jpa-00246467 (part 1, overview)
https://hal.archives-ouvertes.fr/jpa-00246695 (part 2, T surfaces)

lam = free parameter determining the unit cell height
bonnet = Bonnet angle (0 or "D" gives a normal T surface,
                       pi/2 or "P" a tetragonal P surface)
N = grid resolution
pres = presentation of the unit cell.
       For tetragonal P, default unit cell is the "six-ended pipe",
       "square" gives a square catenoid
"""
from mpmath import *
from malibu.trigens import triangle, sector
from malibu.symmetry import pointgroup
from malibu.symmetry.transforms import promote, mprod
from malibu.symmetry.generators import rotate_z, permu, translate

def prepatch(*, lam=-14, bonnet="D", **others):
    if bonnet == "G":
        lam = -14

    lp = lam + sqrt(lam*lam-4)
    ls = sqrt(2-lam)
    gz = sqrt(-lp/2)
    mz = lp*lp / 4
    gx = sqrt(2*ls) # actually 1/gx; also serves as (1/)gy
    mx = 0.5 - 1/ls
    my = 0.5 + 1/ls
    tx = 1 / sqrt(mx)
    ty = 1 / sqrt(my)

    if bonnet in ("D", 0):
        bonnet_v = 1
        scale = gx / ellipk(mx)
    elif bonnet in ("P", pi/2):
        bonnet_v = 1j
        scale = gx / ellipk(my)
    elif bonnet == "G":
        K1, K3 = ellipk(0.25), ellipk(0.75)
        bonnet_v = expj(atan(K1 / K3))
        scale = hypot(K1, K3) / (K1*K3)
    else:
        bonnet_v = expj(bonnet)
        scale = 1

    def patch(w):
        try:
            u = chop(gx * w / sqrt(w**4 + ls*w**2 + 1))
        except ZeroDivisionError:
            u = inf
        if u.imag == 0 and u.real > 1:
            u2 = chop(sqrt(-w**4 + ls*w**2 - 1) / (gx * w))
            if u > tx:
                xF = mpc(ellipf(asin(tx / u).real, mx), ellipk(my))
            else:
                xF = mpc(ellipk(mx), ellipf(asin(ty * u2).real, my))
            if u > ty:
                yF = mpc(ellipf(asin(ty / u).real, my), ellipk(mx))
            else:
                yF = mpc(ellipk(my), ellipf(asin(tx * u2).real, mx))
        else:
            fphi = asin(u)
            xF = ellipf(fphi, mx)
            yF = ellipf(fphi, my)
        v = chop(w*w / gz)
        if v.imag == 0 and v.real > 1:
            v2 = sqrt((v+1)*(v-1) / (1-mz)) / v
            zF = mpc(ellipk(mz), ellipf(asin(v2), 1-mz))
        else:
            zF = ellipf(asin(v), mz)
        x = re(bonnet_v * xF / gx)
        y = im(bonnet_v * yF / gx)
        z = re(bonnet_v * zF * gz)
        return matrix([x, y, z]) * scale
    return patch

def d_aspectratio(lam):
    """Return the ratio of height to base side length for the D
    unit cell with given lam."""
    lp = lam + sqrt(lam*lam-4)
    ls = sqrt(2-lam)
    G = 2/(2-lp) * sqrt(-2*lp*ls)
    return G * ellipk(4/(2-lam)) / ellipk(0.5-1/ls)

def p_aspectratio(lam):
    """Return the ratio of height to base side length for the tetragonal P
    square catenoid unit cell with given lam."""
    lp = lam + sqrt(lam*lam-4)
    ls = sqrt(2-lam)
    G = 2/(2-lp) * sqrt(-lp*ls)
    return G * ellipk((lam+2)/(lam-2)) / ellipk(0.5+1/ls)

def trigen(*, lam=-14, bonnet="D", N=16, **others):
    # Special branch for the gyroid to produce neat unit cells
    if bonnet == "G":
        rf = lambda th: -sqrt(1j) + rect(sqrt(2), th)
        yield from sector(0, rf, pi/6, pi/3, N)
        return
    beta = root(-(lam + sqrt(lam*lam-4)) / 2, 4)
    rf = lambda th: rect(1, th)
    yield from triangle(0, beta, sqrt(1j), N)
    yield from sector(beta, rf, 0, pi/4, N)
    if bonnet not in ("D", "P", 0, pi/2):
        yield from triangle(0, beta*1j, sqrt(1j), N)
        yield from sector(beta*1j, rf, pi/2, pi/4, N)

def syms(*, lam=-14, bonnet="D", pres=None, **others):
    if bonnet == "G":
        lam = -14

    if bonnet in ("D", 0):
        h = d_aspectratio(lam)
        print("unit cell aspect ratio =", h)
        print("unit cell height =", 2*h)
        return mprod([rotate_z(1+1j) / sqrt(2)], pointgroup("Dd", 2),
                      pointgroup("S", 2, basis=promote(x=1, z=h/sqrt(2))))
    if bonnet in ("P", pi/2):
        h = p_aspectratio(lam)
        if pres is None:
            print("unit cell aspect ratio =", sqrt(2)*h)
            print("unit cell height =", 2*sqrt(2)*h)
            return mprod([rotate_z(1+1j) / sqrt(2)], pointgroup("Dh", 4),
                         [translate(-1, -1, h)], [eye(3), diag([-1, 1, -1])])
        if pres == "square":
            print("unit cell aspect ratio =", h)
            print("unit cell height =", 2*h)
            return mprod(pointgroup("Dh", 4), [translate(1, -1, h)])
    if bonnet == "G":
        return mprod(pointgroup("S", 2),
                     [eye(3), promote(permu(1, -2, -3), y=1),
                      promote(permu(-1, 2, -3), z=1), promote(permu(-1, -2, 3), x=1)],
                     [translate(0.5, 0.5, 0.5)],
                     [eye(3), permu(2, 3, 1), permu(3, 1, 2)],
                     pointgroup("S", 2),
                     [eye(3), promote(permu(-2, 1, -3), -0.5, -0.5, 0.5)],
                     [promote(rotate_z(1-1j), 0, -0.5, 0.25)])
    return pointgroup("S", 4)
