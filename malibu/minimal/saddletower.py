"""Data for Karcher's saddle tower.

The Weierstrass–Enneper parametrisation has
f(z) = 1/(z^(2k) + 1), g(z) = z^(k-1)
and k = 2 yields Scherk's second surface.

This is the most symmetric case of a larger family (see saddletower2).
Scaling is performed to make the unit cell height 2.

k = half number of ends; surface has linegroup(13, k) symmetry
cutoff = width of grid before conformal transformation
Nr = grid resolution in real direction
Ni = grid resolution in imaginary direction"""
from mpmath import *
from malibu.trigens import bilinear
from malibu.symmetry import linegroup

def prepatch(*, k=3, **others):
    scale = k/pi
    def patch(w):
        mw2k = -power(w, 2*k)
        P1 = w * hyp2f1(1, 1/(2*k), 1+1/(2*k), mw2k)
        P2 = power(w, 2*k-1) * hyp2f1(1, 1-1/(2*k), 2-1/(2*k), mw2k) / (2*k-1)
        x = re(P1 - P2)
        y = im(P1 + P2)
        z = 0.5 / scale - re(2 * atan(power(w, k)) / k)
        return matrix([x, y, z]) * scale
    return patch

def trigen(*, k=3, cutoff=3, Nr=16, Ni=32, **others):
    cm = lambda z: root(1j * (1-exp(z)) / (1+exp(z)), k)
    yield from bilinear(0, -pi/2j, -cutoff, -cutoff-pi/2j, Nr, Ni, cm)

def syms(*, k=3, **others):
    return linegroup(13, k, 2)
