from mpmath import matrix, linspace, fraction

def bilinear(a, b, c, d, Nu, Nv, fmap=lambda x: x):
    """Yields triangles for the quadrilateral abcd by bilinear interpolation.
    Interpolation is done along the a-b and c-d sides first, with Nu steps,
    giving e and f respectively; interpolation is then performed on e-f
    with Nv steps.
    
    fmap is a final map applied to each point of the bilinear grid.
    This is primarily intended for minimal surfaces, where conformal maps
    produce a very neat render."""
    pts = matrix(Nu+1, Nv+1)
    for (m, u) in enumerate(linspace(0, 1, Nu+1)):
        ab = (1-u)*a + u*b
        cd = (1-u)*c + u*d
        for (n, v) in enumerate(linspace(0, 1, Nv+1)):
            pts[m,n] = fmap((1-v)*ab + v*cd)
    for u in range(Nu):
        for v in range(Nv):
            yield (pts[u,v], pts[u+1,v], pts[u+1,v+1])
            yield (pts[u,v], pts[u+1,v+1], pts[u,v+1])

def sector(origin, curve, a, b, N):
    """Yields triangles for the sector subtended by the given curve
    function, evaluated from a to b, at the origin.
    
    The sector is thought of as comprising N+1 "rings" related by homothety,
    where ring 0 is the origin and ring N is the curve itself. On ring i
    the points 0/i, 1/i, ..., i/i of the way from a to b on the (scaled) curve
    are sampled."""
    pts = matrix(N+1, N+1)
    for (i, r) in enumerate(linspace(0, 1, N+1)):
        for (j, t) in enumerate(linspace(0, 1, i+1)):
            p = curve((1-t)*a + t*b)
            pts[i-j,j] = (1-r)*origin + r*p
    for i in range(N):
        for j in range(N-i):
            yield (pts[i,j], pts[i+1,j], pts[i,j+1])
            if i+j+1 < N:
                yield (pts[i,j+1], pts[i+1,j], pts[i+1,j+1])

def triangle(a, b, c, N):
    """Yields triangles for the triangle abc, evenly spaced in barycentric
    coordinates. N**2 triangles are generated."""
    pts = matrix(N+1, N+1)
    for p in range(N+1):
        for q in range(N-p+1):
            pts[p,q] = fraction(N-p-q, N)*a + fraction(p, N)*b + fraction(q, N)*c
    for p in range(N):
        for q in range(N-p):
            yield (pts[p,q], pts[p+1,q], pts[p,q+1])
            if p+q+1 < N:
                yield (pts[p,q+1], pts[p+1,q], pts[p+1,q+1])
