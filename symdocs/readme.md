The files in this folder relate, in some way or another, to the formalisation of symmetry for Malibu.

----

Some notation in here is new, others quite well-understood, but I have tried to make things as intuitive as possible while still maintaining a good degree of rigour.
