"""
Data for the helicoid/catenoid associate family.

bonnet = Bonnet angle (0 gives catenoid, pi/2 gives helicoid)
R = raw helicoid radius or half-height of raw catenoid
Nr = resolution in radial direction, [-R, R] is divided into Nr steps
Np = resolution in polar direction, [-pi, pi] is divided into Np steps
"""
from mpmath import *
from malibu.trigens import bilinear

def prepatch(*, bonnet, **others):
    bonnet_v = expj(bonnet)
    def patch(w):
        x = re(bonnet_v * cosh(w))
        y = im(bonnet_v * sinh(w))
        z = re(bonnet_v * w)
        return matrix([x, y, z])
    return patch

def trigen(*, R, Nr=30, Np=60, **others):
    z = mpc(R, pi)
    yield from bilinear(z, -conj(z), conj(z), -z, Nr, Np)

def syms(*, R, patch, **others):
    return [eye(3) / norm(patch(R)[:2])]
