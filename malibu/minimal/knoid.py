"""
Data for the k-noid.

The formulas were swiped off Wikipedia.

k = number of ends
cutoff = width of grid before conformal transformation
Nr = grid resolution in real direction
Ni = grid resolution in imaginary direction
"""
from mpmath import *
from malibu.trigens import bilinear
from malibu.symmetry import pointgroup

def prepatch(*, k=3, **others):
    def patch(w):
        wk = power(w, k)
        wk1 = powm1(w, k)
        if w == 0:
            x = 0
            y = 0
        else:
            p1 =  (k-1) * wk1 * hyp2f1(1,-1/k, 1-1/k, wk) - 1 - k*wk1
            p2 = ((k-1) * wk1 * hyp2f1(1, 1/k, 1+1/k, wk) - 1) * w**2
            x = re((p1 - p2) / (k*w*wk1))
            y = im((p1 + p2) / (k*w*wk1))
        z = re((1 + wk) / (k*wk1))
        return matrix([x, y, z])
    return patch

def trigen(*, k=3, cutoff=2, Nr=16, Ni=24, **others):
    cm = lambda z: power((1j-exp(z)) / (1j+exp(z)), 2/k)
    yield from bilinear(0, -pi/2j, -cutoff, -cutoff-pi/2j, Nr, Ni, cm)

def syms(*, k=3, **others):
    return pointgroup("Dh", k)
