"""
Data for Catalan's minimal surface.

This surface solves the Björling problem for a cycloid.
The translation vector between periods is 2*pi.

width = half-width of strip around parametric region
        (i.e. -width <= Im(w) <= width)
Nr = grid resolution in real direction
Ni = grid resolution in imaginary direction
show_cycloid = if True, render half of the surface to show the cycloid
"""
from mpmath import *
from malibu.trigens import bilinear
from malibu.symmetry.generators import permu
from malibu.symmetry import pointgroup

def prepatch(**others):
    def patch(w):
        x = re(w - sin(w))
        y = im(-4*cos(w/2))
        z = re(1 - cos(w))
        return matrix([x, y, z])
    return patch

def trigen(*, width=2, Nr=32, Ni=24, show_cycloid=False, **others):
    yield from bilinear(0, pi, width*1j, pi+width*1j, Nr, Ni)

def syms(show_cycloid=False, **others):
    return pointgroup("Cv", 1 if show_cycloid else 2, basis=permu(2, -1, 3))
