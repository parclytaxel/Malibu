"""
Functions that work with 3D transformations.

Most functions here are generators as the order in which transformations
come is often unimportant – you just want all of them to apply to a surface
via the syms() function. However, since crystallographic transformations
are intimately tied to group theory, Malibu adopts the convention
of always yielding an identity matrix first.
"""
from itertools import product
from functools import reduce
from mpmath import eye

def promote(M=eye(3), x=0, y=0, z=0):
    """M is a 3×3 matrix representing a linear transformation.
    Return the 4×4 block diagonal matrix [M, 1], optionally
    with a translation."""
    res = eye(4)
    res[:3,:3] = M
    res[0,3] = x
    res[1,3] = y
    res[2,3] = z
    return res

def anyproduct2(A, B):
    """A and B are 3×3 or 4×4 matrices in any configuration;
    return an appropriately sized product."""
    if A.cols < B.cols:
        return promote(A) * B
    if A.cols > B.cols:
        return A * promote(B)
    return A * B

def anyproduct(seq):
    return reduce(anyproduct2, seq)

def mprod(*seqs):
    """Given several iterables of matrices, take one matrix
    from each iterable like itertools.product() does, multiply together
    and yield the results.

    The initial m may be understood as "matrix", "multiply"
    and "multiple" (sequences of matrices)."""
    yield from map(anyproduct, product(*seqs))

def changebase_deco(func):
    """Decorator that adds a "basis" keyword argument to any generator yielding
    transformation matrices, allowing arbitrary bases.
    
    The basis represents an OTHER coordinate system in the CURRENT coordinates.
    If M is an output of the undecorated function, the decorated function returns
    in the CURRENT coordinates the action of performing M under the OTHER coordinates."""
    def outfunc(*args, basis=None, **kwargs):
        if basis is None:
            yield from func(*args, **kwargs)
        else:
            for M in func(*args, **kwargs):
                yield anyproduct((basis, M, basis**-1))
    return outfunc
