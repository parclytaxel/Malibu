"""
Data for the Schwarz CLP surface.

The Weierstrass–Enneper parametrisation is the same as for the T family:
g(w) = w, f(w) = 1/sqrt(w^8 + lam*w^4 + 1)
But now lam is in (-2, 2); lam = 2 gives Scherk's second surface.
The surfaces are conjugate to themselves, with the lam = 0 case self-conjugate.
They have space group P4_2/mcm (#132).

Like the T surfaces, the unit cells are scaled to have base side length 2
and the function clp_aspectratio() can be imported alone.

Equations from Cvijović and Klinowski's papers:
https://hal.archives-ouvertes.fr/jpa-00246467 (part 1, overview)
https://hal.archives-ouvertes.fr/jpa-00246696 (part 3, CLP surfaces)

lam = free parameter determining the unit cell height
N = grid resolution
"""
from mpmath import *
from malibu.trigens import sector
from malibu.symmetry import linegroup

def prepatch(*, lam=0, **others):
    ls = sqrt(2-lam)
    gz = 2 / (2+ls)
    mz = (8*ls) / (2+ls)**2
    zc = sqrt(2+lam) / (2-ls)
    gx = sqrt(2+ls) # actually 1/gx; also serves as (1/)gy
    mx = (2-ls) / (2+ls)
    my = (2*ls) / (2+ls)
    tx = 1 / sqrt(mx)
    ty = 1 / sqrt(my)
    scale = gx / ellipk(mx)

    def patch(w):
        u = chop(gx * w / sqrt(w**4 + ls*w**2 + 1))
        if u.imag == 0 and u.real > 1:
            u2 = ((w**2 - 1) / (gx * w)).imag
            if u > ty:
                yF = mpc(ellipf(asin(ty / u), my), ellipk(mx))
            else:
                yF = mpc(ellipk(my), ellipf(asin(tx * u2), mx))
        else:
            yF = ellipf(asin(u), my)
        v = chop(zc * (1 - w**2) / (1 + w**2))
        if v.real == 0 and v.imag < -1:
            vi = v.imag
            v2 = -vi / sqrt((vi+1)*(vi-1))
            zF = mpc(ellipf(asin(1 / (sqrt(mz)*v2)), mz), ellipk(1-mz))
        else:
            zF = ellipf(atan(v), mz)
        x = re(ellipf(asin(gx * w / (w**2 + 1)), mx)) / gx
        y = im(yF) / gx
        z = re(zF) * gz
        return mp.matrix([x, y, z]) * scale
    return patch

def clp_aspectratio(lam):
    """Return the ratio of height to base side length for the
    CLP unit cell with given lam."""
    ls = sqrt(2-lam)
    return sqrt(2+ls) * ellipk(0.5-lam/4) / ellipk((2-ls)/(2+ls))

def trigen(*, lam=0, N=16, **others):
    pole = arg(root(mpc(-lam, sqrt(4-lam*lam)) / 2, 4))
    rf = lambda th: rect(1, th)
    yield from sector(0, rf, 0, pole, N)
    yield from sector(0, rf, pole, pi/4, N)

def syms(*, lam=0, **others):
    a = clp_aspectratio(lam)
    print("unit cell aspect ratio =", a)
    print("unit cell height =", 2*a)
    return linegroup(13, 2, 2*a)
