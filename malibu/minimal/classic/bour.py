"""
Data for Bour's minimal surface.

R = grid radius
N = grid resolution
"""
from mpmath import *
from malibu.trigens import sector
from malibu.symmetry import pointgroup

def prepatch(**others):
    def patch(w):
        x = re(w*(1-w/2))
        y = im(w*(1+w/2))
        z = 4 * re(power(w, 1.5)) / 3
        return matrix([x, y, z])
    return patch

def trigen(*, R=0.5, N=32, **others):
    arc = lambda th: rect(R, th)
    yield from sector(0, arc, 0, pi/3, N)

def syms(**others):
    return pointgroup("Dh", 3)
