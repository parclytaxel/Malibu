"""
Data for the Richmond surface.

This surface has one planar and one Enneper end. It has the
Weierstrass–Enneper data f(z) = 1/z^2, g(z) = z^k.

k = 1/4 of symmetry group order, or exponent in
    Weierstrass–Enneper parametrisation MINUS one
cutoff_l, cutoff_r = left and right borders of grid before
                     conformal transformation (left = planar end,
                     right = Enneper end)
Nr = grid resolution in real direction
Ni = grid resolution in imaginary direction
"""
from mpmath import *
from malibu.trigens import bilinear
from malibu.symmetry import pointgroup

def prepatch(*, k=2, **others):
    def patch(w):
        v = power(w, 2*k+1) / (2*k+1)
        x = re(-1/w - v)
        y = im(-1/w + v)
        z = re(2 * power(w, k) / k)
        return matrix([x, y, z])
    return patch

def trigen(*, k=2, cutoff_l=1.2, cutoff_r=0.2, Nr=16, Ni=24, **others):
    yield from bilinear(cutoff_r, cutoff_r-pi/(2j*k),
                        -cutoff_l, -cutoff_l-pi/(2j*k), Nr, Ni, exp)

def syms(*, k=2, **others):
    return pointgroup("Dd", k)
