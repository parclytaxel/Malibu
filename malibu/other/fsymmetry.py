"""
A little Malibu script that visualises symmetries.

The base object is a little F with a nub on its top-left corner
to make it chiral in 3D. It lies in the positive octant,
the preferred position for many of the group-producing functions
in Malibu.

pos = offset of the nubbed F's bounding-box centre as a triple of floats,
      to allow for less cluttered renders
scale = scale factor of the nubbed F
sym = symmetry to realise
"""
from mpmath import matrix

def prepatch(pos=(0.8, 0.5, 0.3), scale=1, **others):
    F_verts = ((0.0, 0.05, -0.05), (0.0, 0.0, 0.0), (0.0, -0.05, -0.05),
               (0.0, -0.05, 0.05), (0.0, -0.05, 0.15), (0.0, -0.15, 0.15),
               (0.0, -0.15, -0.25), (0.0, -0.05, -0.25), (-0.1, -0.05, 0.15),
               (-0.1, 0.15, 0.15), (0.0, 0.15, 0.15), (-0.1, -0.05, 0.05),
               (-0.1, 0.05, 0.05), (-0.1, 0.05, -0.05), (-0.1, -0.05, -0.05),
               (-0.1, -0.05, -0.25), (-0.1, -0.15, -0.25), (-0.1, 0.15, 0.25),
               (-0.1, -0.15, 0.25), (0.0, 0.15, 0.25), (0.0, -0.05, 0.25),
               (0.1, -0.15, 0.25), (0.1, -0.05, 0.25), (0.1, -0.15, 0.15),
               (0.0, 0.05, 0.05), (0.1, -0.05, 0.15))
    mpos = matrix(pos)
    def patch(w):
        return matrix(F_verts[w]) * scale + mpos
    return patch

def trigen(**others):
    F_tris = ((0, 1, 2), (3, 4, 5), (1, 3, 2), (6, 7, 2), (3, 5, 6), (2, 3, 6),
              (4, 8, 9), (4, 9, 10), (11, 12, 13), (14, 15, 16), (11, 13, 14),
              (17, 9, 8), (11, 14, 16), (18, 17, 8), (8, 11, 16), (16, 18, 8),
              (9, 17, 19), (9, 19, 10), (20, 4, 10), (20, 10, 19), (20, 19, 17),
              (18, 21, 22), (20, 17, 18), (20, 18, 22), (8, 4, 3), (8, 3, 11),
              (18, 16, 6), (5, 23, 21), (18, 6, 5), (18, 5, 21), (14, 2, 7),
              (14, 7, 15), (2, 14, 13), (2, 13, 0), (12, 24, 0), (12, 0, 13),
              (11, 3, 24), (11, 24, 12), (15, 7, 6), (15, 6, 16), (25, 22, 21),
              (25, 21, 23), (4, 20, 22), (4, 22, 25), (5, 4, 25), (5, 25, 23),
              (0, 24, 1), (1, 24, 3))
    yield from F_tris

def syms(*, sym, **others):
    return sym
