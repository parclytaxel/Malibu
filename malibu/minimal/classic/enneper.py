"""
Data for the Enneper surface.

k = 1/4 of symmetry group order, or exponent in
    Weierstrass–Enneper parametrisation plus one
R = grid radius
N = grid resolution
"""
from mpmath import *
from malibu.trigens import sector
from malibu.symmetry import pointgroup

def prepatch(*, k=2, **others):
    def patch(w):
        v = power(w, 2*k-1) / (2*k-1)
        x = re(w - v)
        y = im(w + v)
        z = re(2 * power(w, k) / k)
        return matrix([x, y, z])
    return patch

def trigen(*, k=2, R=2, N=32, **others):
    arc = lambda th: rect(R, th)
    yield from sector(0, arc, 0, pi/(2*k), N)

def syms(*, k=2, **others):
    return pointgroup("Dd", k)
