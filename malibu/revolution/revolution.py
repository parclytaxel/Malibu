"""
Data for an arbitrary surface of revolution.

If the curve function is (x(t), y(t)), the surface is parametrised as
(x(t)*cos(phi), x(t)*sin(phi), y(t))
where a <= t <= b and 0 <= phi <= 2*pi
(i.e. curve is rotated about the z-axis).

curve = curve to rotate; input is real t, output is complex(x(t), y(t))
curve_a, curve_b = lower/upper bounds of range of t
Nphi = resolution around symmetry axis
Nt = resolution in the direction of t
"""
from functools import lru_cache
from malibu.trigens import bilinear
from mpmath import pi, matrix, rect

def prepatch(*, curve, **others):
    curve = lru_cache(None)(curve)
    def patch(w):
        phi, t = w.real, w.imag
        a = curve(t)
        b = rect(a.real, phi)
        return matrix([b.real, b.imag, a.imag])
    return patch

def trigen(*, curve_a, curve_b, Nphi=60, Nt=30, **others):
    l, r, d, u = 0, 2*pi, curve_a*1j, curve_b*1j
    yield from bilinear(l+d, r+d, l+u, r+u, Nphi, Nt)
