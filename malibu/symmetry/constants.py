"""
Specific sequences of transformation matrices and other relevant data.
"""
from mpmath import matrix, eye, phi
from .generators import permu

flip_z = (eye(3), permu(1, 2, -3))
flip_y = (eye(3), permu(1, -2, 3))
turn_x = (eye(3), permu(1, -2, -3))
swap_xy = (eye(3), permu(2, 1, 3))
cycle_axes = (eye(3), permu(2, 3, 1), permu(3, 1, 2))

fee = phi - 1
pent1 = matrix([[1, -phi, fee], [phi, fee, -1], [fee, 1, phi]]) / 2
pent2 = matrix([[-fee, -1, phi], [1, -phi, -fee], [phi, fee, 1]]) / 2
cycle_pent = (eye(3), pent1, pent2, pent2.T, pent1.T)
