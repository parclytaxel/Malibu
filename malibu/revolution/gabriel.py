"""
Data for Gabriel's Horn.
"""
from mpmath import mpc
from .revolution import prepatch as pp, trigen as tg

def prepatch(**others):
    curve = lambda t: mpc(1 / t, t - 1)
    return pp(curve=curve, **others)
trigen = tg
