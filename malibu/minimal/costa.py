"""
Data for Costa's minimal surface.

Equations from Ferguson, Gray and Markvorsen, Costa's Minimal Surface
via Mathematica, https://www.researchgate.net/publication/254465398

Setting up the input domain here is unusually involved. It is a right
isosceles triangle with vertices at 0, 0.5 and 0.5+0.5j, punctured at
the first two points. This cannot be easily expressed with one mesh
generator, so the triangle is split into three domains:

        .
       /|
      / |
     .  .
    /| /|
   / |/ |
  o--.--o

The puncture at 0 represents the planar end, that at 0.5 the catenoid end.

The central parallelogram is parametrised normally, with even spacing
between the points. The other two punctured triangles use their own
conformal maps to accommodate the punctures; because they have to match
up with the parallelogram points, things get quite tricky.

N1 = grid resolution of parallelogram in / direction
N2 = grid resolution of parallelogram in | direction
N3 = grid resolution of triangle punctured at 0 (N2*N3)
N4 = grid resolution of triangle punctured at 0.5 (N1*N4)
c1 = width of grid generating the 0-punctured triangle
c2 = width of grid generating the 0.5-punctured triangle
"""
from mpmath import *
from malibu.trigens import bilinear
from malibu.symmetry import pointgroup

def prepatch(**others):
    e1 = gamma(0.25)**4 / (8*pi)
    f1 = gamma(0.25)**2 / (2*sqrt(pi))
    xy_delta = pi**2 / (8*e1)
    zeta_mul = pi / (2*e1)
    z_scale = sqrt(2*pi) / 4

    def weierp_costa(z):
        return e1 * chop(2 / ellipfun("sn", f1*z, 0.5)**2 - 1)

    def weierz_costa(z):
        return pi*z + diff(lambda u: log(jtheta(1, pi*u, exp(-pi))), z)

    def patch(w):
        Z1 = weierz_costa(w)
        Z2 = zeta_mul * (weierz_costa(w - 0.5) - weierz_costa(w - 0.5j))
        P1 = weierp_costa(w)
        x = 0.5 * re(pi*w + Z2 - Z1) + xy_delta
        y = 0.5 * im(pi*w + Z2 + Z1) + xy_delta
        z = z_scale * log(abs((P1 - e1)/(P1 + e1)))
        return matrix([x, y, z])
    return patch

def trigen(*, N1=16, N2=16, N3=16, N4=16, c1=0.8, c2=1.5, **others):
    yield from bilinear(0.5+0.25j, 0.25, 0.5+0.5j, 0.25+0.25j, N1, N2)
    def cm1(z):
        x, y = z.real, z.imag
        y = atan2(y, 0.25)
        scale = 1 - log(cos(y)) / c1
        return exp(mpc(x * scale - c1, y)) / 4
    yield from bilinear(c1, 0, c1+0.25j, 0.25j, N3, N2, cm1)
    def cm2(z):
        x, y = z.real, z.imag
        th = atan2(y, 1-y)
        delta = log(hypot(y, 1-y) * sqrt(2))
        scale = 1 + delta / c2
        return exp(mpc(x * scale - c2, th)) * sqrt(-1/32) + 0.5
    yield from bilinear(c2, 0, c2+1j, 1j, N4, N1, cm2)

syms = tuple(pointgroup("Dd", 2))
