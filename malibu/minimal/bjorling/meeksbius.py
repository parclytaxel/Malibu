"""
Data for Meeks's minimal Möbius strip.

This solves the Björling problem for a circle with a normal that rotates half
a turn per revolution. Its Weierstrass–Enneper parametrisation
(see http://www.ugr.es/~jperez/papers/bamsJan11.pdf) is
f(z) = h(z) / g(z), g(z) = z^2 * (z+1)/(z-1), h(z) = i*(1-1/z^2)

width = half-width of strip before exponential map
Nr = grid resolution in real direction
Ni = grid resolution in imaginary direction
"""
from mpmath import *
from malibu.trigens import bilinear

def prepatch(**others):
    def patch(w):
        p1 = ((w/3 + 1)*w + 1)*w
        p2 = ((1 - 1/(3*w))/w - 1)/w
        x = re(p1 + p2)
        y = im(p1 - p2)
        z = im(2*(w + 1/w))
        return matrix([x, y, z])
    return patch

def trigen(*, width=0.5, Nr=16, Ni=48, **others):
    yield from bilinear(-width, width, -width+1j*pi, width+1j*pi, Nr, Ni, exp)
