"""
Data for a pseudosphere.
"""
from mpmath import mpc, sech, tanh
from .revolution import prepatch as pp, trigen as tg

def prepatch(**others):
    curve = lambda t: mpc(sech(t), t - tanh(t))
    return pp(curve=curve, **others)
trigen = tg
