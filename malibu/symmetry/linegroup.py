"""
Line groups in three dimensions.
"""
import re
from mpmath import eye
from .transforms import promote, mprod, changebase_deco
from .generators import rotate_z
from .constants import turn_x, flip_y
from .pointgroup import pointgroup

non_hm_symbols = re.compile(r"[^1-6_/mc-]")
kind_descs = ("C", "S", "Ch", "Cv", "D", "Dd", "Dh",
              "zCh", "pCv", "zCv", "pDd", "pDh", "zDh")
kind_ops = (("C", 0), ("S", 0), ("Ch", 0), ("Cv", 0), ("D", 0), ("Dd", 0), ("Dh", 0),
            ("Ch", "h"), ("C", "v"), ("Cv", "s"), ("D", "h"), ("Ch", "v"), ("Dh", "h"))

def glide(kind, n, a):
    """Generate matrices expressing the initial "glide" required for
    the generation of groups 8 to 13."""
    yield eye(3)
    if kind == "h":
        yield promote(rotate_z((2*n, -1), -1), z=a/2)
    elif kind == "v":
        yield promote(flip_y[1], z=-a/2)
    elif kind == "s":
        yield promote(rotate_z((2*n, -1)), z=-a/2)

@changebase_deco
def linegroup(kind, n, a=1, q=0):
    """Yield matrices describing the specified line group.
    
    kind is an integer in [1, 13] or a string descriptor,
    n is the order, a is the unit cell height. For type 1 and 5 groups,
    an integral q implements the Hermann–Mauguin symbol n_q and
    irrational helices otherwise; see symdocs/linegroups.md
    for more information."""
    if type(kind) == str:
        kind = kind_descs.index(kind) + 1
    if (kind == 1 or kind == 5) and q != 0:
        if integral_q := type(q) == int:
            h = n//2
            gen = (promote(rotate_z((n, k)), z=((k*q+h) % n - h) * a/n) for k in range(n))
        else:
            gen = (promote(rotate_z(q**k), z=k*a) for k in range(n))
        if kind == 1:
            yield from gen
        elif integral_q:
            yield from mprod(turn_x, gen)
        else:
            yield from mprod(gen, turn_x)
        return
    if kind == 2:
        n *= 2
    pgroup, gtype = kind_ops[kind-1]
    yield from mprod(pointgroup(pgroup, n), glide(gtype, n, a))

rod_data = {
# Triclinic
1: (1, 1), "1": 1,
2: (2, 1), "-1": 2,
# Monoclinic
3: (5, 1), "211": 3,
4: (4, 1), "m11": 4,
5: (9, 1), "c11": 5,
6: (6, 1), "2/m11": 6,
7: (11, 1), "2/c11": 7,
8: (1, 2), "112": 8,
9: (1, 2, 1), "112_1": 9,
10: (3, 1), "11m": 10,
11: (3, 2), "112/m": 11,
12: (8, 1), "112_1/m": 12,
# Orthorhombic
13: (5, 2), "222": 13,
14: (5, 2, 1), "222_1": 14,
15: (4, 2), "mm2": 15,
16: (9, 2), "cc2": 16,
17: (10, 1), "mc2_1": 17,
18: (7, 1), "2mm": 18,
19: (12, 1), "2cm": 19,
20: (7, 2), "mmm": 20,
21: (12, 2), "ccm": 21,
22: (13, 1), "mcm": 22,
# Tetragonal
23: (1, 4), "4": 23,
24: (1, 4, 1), "4_1": 24,
25: (1, 4, 2), "4_2": 25,
26: (1, 4, 3), "4_3": 26,
27: (2, 2), "-4": 27,
28: (3, 4), "4/m": 28,
29: (8, 2), "4_2/m": 29,
30: (5, 4), "422": 30,
31: (5, 4, 1), "4_122": 31,
32: (5, 4, 2), "4_222": 32,
33: (5, 4, 3), "4_322": 33,
34: (4, 4), "4mm": 34,
35: (10, 2), "4_2cm": 35, "4_2mc": 35,
36: (9, 4), "4cc": 36,
37: (6, 2), "-42m": 37, "-4m2": 37,
38: (11, 2), "-42c": 38, "-4c2": 38,
39: (7, 4), "4/mmm": 39,
40: (12, 4), "4/mcc": 40,
41: (13, 2), "4_2/mmc": 41, "4_2/mcm": 41,
# Trigonal
42: (1, 3), "3": 42,
43: (1, 3, 1), "3_1": 43,
44: (1, 3, 2), "3_2": 44,
45: (2, 3), "-3": 45,
46: (5, 3), "312": 46, "321": 46,
47: (5, 3, 1), "3_112": 47, "3_121": 47,
48: (5, 3, 2), "3_212": 48, "3_221": 48,
49: (4, 3), "3m1": 49, "31m": 49,
50: (9, 3), "3c1": 50, "31c": 50,
51: (6, 3), "-3m1": 51, "-31m": 51,
52: (11, 3), "-3c1": 52, "-31c": 52,
# Hexagonal
53: (1, 6), "6": 53,
54: (1, 6, 1), "6_1": 54,
55: (1, 6, 2), "6_2": 55,
56: (1, 6, 3), "6_3": 56,
57: (1, 6, 4), "6_4": 57,
58: (1, 6, 5), "6_5": 58,
59: (3, 3), "-6": 59,
60: (3, 6), "6/m": 60,
61: (8, 3), "6_3/m": 61,
62: (5, 6), "622": 62,
63: (5, 6, 1), "6_122": 63,
64: (5, 6, 2), "6_222": 64,
65: (5, 6, 3), "6_322": 65,
66: (5, 6, 4), "6_422": 66,
67: (5, 6, 5), "6_522": 67,
68: (4, 6), "6mm": 68,
69: (9, 6), "6cc": 69,
70: (10, 3), "6_3mc": 70, "6_3cm": 70,
71: (7, 3), "-6m2": 71, "-62m": 71,
72: (12, 3), "-6c2": 72, "-62c": 72,
73: (7, 6), "6/mmm": 73,
74: (12, 6), "6/mcc": 74,
75: (13, 3), "6_3/mmc": 75, "6_3/mcm": 75
}

@changebase_deco
def rodgroup(kind, a=1):
    """Yield matrices describing the specified rod group.

    kind is an integer in [1, 75] or a Hermann–Mauguin symbol,
    a is the unit cell height."""
    if type(kind) == str:
        kind = rod_data[non_hm_symbols.sub("", kind.lower())]
    key = list(rod_data[kind])
    key.insert(2, a)
    yield from linegroup(*key)
